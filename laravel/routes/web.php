<?php

use App\Http\Controllers\BiodataController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[DashboardController::class,'home']);
Route::get('/daftar',[BiodataController::class,'daftar']);
Route::post('/home',[BiodataController::class,'welcome']);
Route::get('/data_table',function(){
return view('page.data_table');
});
Route::get('/table',function(){
return view('page.table');
});
